# condex-eval

Yet another Conditional Comment expression evaluator.

## basic usage
```rust
use std::collections::BTreeMap;
use condex;

fn main() {

  let parser = condex::ExprParser::new();

  let dict = {
    let mut dict = BTreeMap::new();
    dict.insert(String::from("mso"), 1.0);
    dict.insert(String::from("vml"), 1.0);
    dict
  }; 

  let mso_compatible = parser.parse("gte mso 1").unwrap().eval(&dict);
  let vml_supports = parser.parse("gte vml 1").unwrap().eval(&dict);

  let vml_and_mso = parser.parse("(gte vml 1) & (gte mso 1)").unwrap().eval(&dict);

  assert_eq!(mso_compatible & vml_supports, vml_and_mso)
}

```
