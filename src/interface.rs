use std::collections::BTreeMap;

use super::ExprParser;
use super::parser::{Parser, Segment};

pub struct Condex<T>
where
  T: Iterator<Item=u8> {
  parser: Parser<T>,
  cached: String,
}


impl<T> Condex<T>
where
  T: Iterator<Item=u8> {

  pub fn new(iter: T) -> Condex<T> {
    Condex {
      parser: Parser::new(iter),
      cached: String::new(),
    }
  }

  pub fn eval(&mut self, dict: &BTreeMap<String, f32>) -> &str {
    let segments = self.parser.parse();

    let expr_parser = ExprParser::new();
    
    let result = segments
      .iter()
      .filter_map(|seg| {
        match seg {
          Segment::Text(ref bytes) => Some(String::from_utf8_lossy(bytes).into_owned()),
          Segment::Condition { ref expr, ref text } => {
            let expr = String::from_utf8_lossy(expr).into_owned();
            if expr_parser.parse(&expr).map(|ast| ast.eval(dict)).unwrap_or(false) {
              Some(String::from_utf8_lossy(text).into_owned())
            } else {
              None
            }
          }
        }
      })
      .collect();
    
    self.cached = result;
    self.result()
  }

  pub fn result(&self) -> &str {
    &self.cached
  }
}
