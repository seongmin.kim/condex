use super::{ast, ExprParser};
use super::Condex;

#[test]
fn parse_scalar_values() {
  use ast::Expr;

  let parser = ExprParser::new();

  assert_eq!(parser.parse("10").unwrap(), Box::new(Expr::Number(10.0)));
  assert_eq!(parser.parse("(10)").unwrap(), Box::new(Expr::Number(10.0)));
  assert_eq!(parser.parse("vml").unwrap(), Box::new(Expr::Identifier("vml".to_string())));

}


#[test]
fn parse_comparasons() {
  use ast::{Expr, Comparator};

  let parser = ExprParser::new();

  assert_eq!(
    parser.parse("gte vml 1").unwrap(),
    Box::new(
      Expr::Comparator(
        Comparator::GTE(
          Box::new(Expr::Identifier("vml".to_string())),
          Box::new(Expr::Number(1.0))
        )
      )
    )
  );

}

#[test]
fn parse_complex() {
  use ast::{Expr, Logical, Comparator};

  let parser = ExprParser::new();

  assert_eq!(
    parser.parse("true & (gte vml 1)").unwrap(),

    Box::new(Expr::Logical(
      Logical::AND(
        Box::new(Expr::Boolean(true)),
        Box::new(Expr::Comparator(
          Comparator::GTE(
            Box::new(Expr::Identifier("vml".to_string())),
            Box::new(Expr::Number(1.0))
          )
        ))
      )
    ))
  );

  assert_eq!(
    parser.parse("(gte vml 1) & (lte mso 1)").unwrap(),

    Box::new(Expr::Logical(
      Logical::AND(
        Box::new(Expr::Comparator(
          Comparator::GTE(
            Box::new(Expr::Identifier("vml".to_string())),
            Box::new(Expr::Number(1.0))
          )
        )),
        Box::new(Expr::Comparator(
          Comparator::LTE(
            Box::new(Expr::Identifier("mso".to_string())),
            Box::new(Expr::Number(1.0))
          )
        )),
      )
    ))
  );

  assert_eq!(
    parser.parse("lt mso 8.0").unwrap(),

    Box::new(Expr::Comparator(
      Comparator::LT(
        Box::new(Expr::Identifier("mso".to_string())),
        Box::new(Expr::Number(8.0)),
      )
    ))
  )
}

#[test]
fn test_eq() {
  use ast::{Expr, Comparator};

  let parser = ExprParser::new();

  assert_eq!(
    parser.parse("mso 1").unwrap(),
    Box::new(Expr::Comparator(
      Comparator::EQ(
        Box::new(Expr::Identifier("mso".to_string())),
        Box::new(Expr::Number(1.0)),
      )
    ))
  )
}

#[test]
fn test_eval() {
  let parser = ExprParser::new();

  let mut dict = std::collections::BTreeMap::new();
  dict.insert("mso".to_string(), 12.0);
  dict.insert("vml".to_string(), 1.0);

  assert_eq!( parser.parse("gte mso 1").unwrap().eval(&dict), true);
  assert_eq!( parser.parse("vml 1").unwrap().eval(&dict), true);
  assert_eq!( parser.parse("!(lt mso 8.0)&(gte vml 0.5)&(lte vml 2)").unwrap().eval(&dict), true);
}


#[test]
fn test_parsing() {
  let text = r#"
  <!--[if gte vml 1]>
  <p>I can support vml</p>
  <![endif]>
  <![if !vml]>
  <p>I can not support vml</p>
  <![endif]>
  "#;


  let mut dict = std::collections::BTreeMap::new();
  dict.insert("vml".to_string(), 1.0);

  let mut condex = Condex::new(text.bytes());
  let result = condex.eval(&dict);

  println!("{}", result);
}

#[test]
#[allow(non_snake_case)]
fn hwp_supportEmptyParas() {
  let text = "<!--[if !supportEmptyParas]-->&nbsp;<!--[endif]-->hello";

  let dict = std::collections::BTreeMap::<String, f32>::new();
  let mut condex = Condex::new(text.bytes());
  let result = condex.eval(&dict);

  assert_eq!(result, "<!--[if !supportEmptyParas]-->&nbsp;<!--[endif]-->hello");
}
