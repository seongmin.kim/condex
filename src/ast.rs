use std::collections::BTreeMap;

#[derive(Debug, PartialEq)]
pub enum Expr {
  Logical(Logical),
  Comparator(Comparator),
  Invert(Box<Expr>),

  Boolean(bool),
  Number(f32),
  Identifier(String),
}

impl Expr {
  pub fn eval(self: Box<Self>, map: &BTreeMap<String, f32>) -> bool {
    Evaluate::eval(self, map) != 0.0
  }
}

#[derive(Debug, PartialEq)]
pub enum Comparator {
  GT(Box<Expr>, Box<Expr>),
  GTE(Box<Expr>, Box<Expr>),
  LT(Box<Expr>, Box<Expr>),
  LTE(Box<Expr>, Box<Expr>),
  EQ(Box<Expr>, Box<Expr>),
}

#[derive(Debug, PartialEq)]
pub enum Logical {
  AND(Box<Expr>, Box<Expr>),
  OR(Box<Expr>, Box<Expr>),
}

trait Evaluate {
  fn eval(self, map: &BTreeMap<String, f32>) -> f32;
}

fn into_f32(value: bool) -> f32 {
  if value {
    1.0
  } else {
    0.0
  }
}

impl Evaluate for Box<Expr> {
  fn eval(self, map: &BTreeMap<String, f32>) -> f32 {
    match *self {
      Expr::Logical(pred) => {
        Evaluate::eval(pred, map)
      }
      Expr::Comparator(cmp) => {
        Evaluate::eval(cmp, map)
      }
      Expr::Invert(expr) => {
        let value = Evaluate::eval(expr, map);
        if value != 0.0 && value.is_nan() == false {
          0.0
        } else {
          1.0
        }
      }
      Expr::Boolean(val) => if val { 1.0 } else { 0.0 },
      Expr::Number(num) => num,
      Expr::Identifier(ident) => *map.get(&ident).unwrap_or(&0.0),
    }
  }
}

impl Evaluate for Logical {
  fn eval(self, map: &BTreeMap<String, f32>) -> f32 {
    match self {
      Logical::AND(lhs, rhs) => {
        let lhs = Evaluate::eval(lhs, map);
        let lhs = lhs != 0.0 && lhs.is_nan() == false;

        let rhs = Evaluate::eval(rhs, map);
        let rhs = rhs != 0.0 && rhs.is_nan() == false;

        if lhs && rhs {
          1.0
        } else {
          0.0
        }
      }
      Logical::OR(lhs, rhs) => {
        let lhs = Evaluate::eval(lhs, map);
        let lhs = lhs != 0.0 && lhs.is_nan() == false;

        let rhs = Evaluate::eval(rhs, map);
        let rhs = rhs != 0.0 && rhs.is_nan() == false;

        if lhs || rhs {
          1.0
        } else {
          0.0
        }
      }
    }
  }
}

impl Evaluate for Comparator {
  fn eval(self, map: &BTreeMap<String, f32>) -> f32 {
    match self {
      Comparator::GT(lhs, rhs) => {
        into_f32(Evaluate::eval(lhs, map) > Evaluate::eval(rhs, map))
      }
      Comparator::GTE(lhs, rhs) => {
        into_f32(Evaluate::eval(lhs, map) >= Evaluate::eval(rhs, map))
      }
      Comparator::LT(lhs, rhs) => {
        into_f32(Evaluate::eval(lhs, map) < Evaluate::eval(rhs, map))
      }
      Comparator::LTE(lhs, rhs) => {
        into_f32(Evaluate::eval(lhs, map) <= Evaluate::eval(rhs, map))
      }
      Comparator::EQ(lhs, rhs) => {
        into_f32(Evaluate::eval(lhs, map) == Evaluate::eval(rhs, map))
      }
    }
  }
}
