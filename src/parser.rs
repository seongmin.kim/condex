#![allow(dead_code)]

pub enum Segment {
	Text(Vec<u8>),
	Condition {
		expr: Vec<u8>,
		text: Vec<u8>,
	}	
}


pub struct Parser<T>
where
	T: Iterator<Item=u8>
{
	iter: T,
	current_expr: Vec<u8>,
	current_text: Vec<u8>,
	restore_buffer: Vec<u8>,
	look: [Option<u8>; 3],
	segments: Vec<Segment>,
}


impl<T> Parser<T>
where
	T: Iterator<Item=u8>
{
	pub fn new(iter: T) -> Parser<T> {
		let mut lxr = Parser {
			iter,
			current_expr: vec![],
			current_text: vec![],
			restore_buffer: vec![],
			look: [None; 3],
			segments: vec![],
		};
		lxr.shift(3);
		lxr
	}

	#[inline]
	fn shift(&mut self, n: usize) {
		for _ in 0..n {
			self.next_byte();
		}
	}

	#[inline]
	fn next_byte(&mut self) -> Option<u8> {
		self.look.rotate_left(1);
		let last = self.look.last_mut().unwrap();
		let mut next = self.iter.next();
		std::mem::swap(last, &mut next);
		next
	}

	#[inline]
	fn stack_segment(&mut self, segment: Segment) {
		self.segments.push(segment);
	}

	#[inline]
	fn emit(&mut self) {
		let mut expr = Vec::new();
		let mut text = Vec::new();
		std::mem::swap(&mut expr, &mut self.current_expr);
		std::mem::swap(&mut text, &mut self.current_text);

		if text.is_empty() == false {
			
			let segment = if expr.is_empty() {
				Segment::Text ( text )
			} else {
				Segment::Condition { expr, text }
			};

			self.stack_segment(segment);

			// let restore = self.restore_buffer.drain(..).collect::<Vec<_>>();
			// self.stack_segment(Segment::Text(restore));
		}		
	}

	#[inline]
	fn push_text(&mut self, text: &[u8]) {
		self.current_text.extend(text);
	}

	#[inline]
	fn push_expr(&mut self, expr: &[u8]) {
		self.current_expr.extend(expr);
	}

	#[inline]
	fn push_buffer(&mut self, front: &[u8]) {
		self.restore_buffer.extend(front);
	}

	#[inline]
	fn clear_buffer(&mut self) {
		self.restore_buffer.clear();
	}

	#[inline]
	fn drain_expr(&mut self) {
		self.restore_buffer.append(&mut self.current_expr);
	}

	#[inline]
	fn drain(&mut self) {
		self.current_text.append(&mut self.restore_buffer);
	}

	#[inline]
	fn predict(&self, bytes: &[u8]) -> bool {
		let len = bytes.len();
		let next = self.look.to_vec().into_iter().flatten().collect::<Vec<u8>>();
		len <= next.len() && bytes == &next[..len]
	}

	pub fn parse(&mut self) -> &[Segment] {
		while let Some(byte) = self.next_byte() {
			match byte {
				b'<' => {
					if let Some(next) = self.next_byte() {
						match next {
							b'!' => self.step_declamation(),
							t => self.push_text(&[b'<', t]),
						}
					}
				}
				t => self.push_text(&[t]),
			}
		}
		self.emit();
		&self.segments
	}

	fn step_declamation(&mut self) {
		self.push_buffer(b"<!");

		if let Some(byte) = self.next_byte() {
			match byte {
				b'[' => self.step_bracket(),
				b'-' => {
					if self.predict(b"-[") {
						self.shift(2);
						self.push_buffer(b"--");
						self.step_bracket();
					} else {
						self.push_buffer(b"-");
					}
				}
				t => self.push_buffer(&[t]),
			}
		}

		self.drain();
	}	

	fn step_bracket(&mut self) {
		self.push_buffer(b"[");

		if self.predict(b"if") && self.look[2].map_or(false, |b| b.is_ascii_whitespace()) {
			self.emit();
			self.push_buffer(&[b'i', b'f', self.look[2].unwrap()]);
			self.shift(3);
			
			self.step_condition();
		} else {
			let maybe_endif = {
				let mut buff = vec![];
				for _ in 0..5 {
					buff.push(self.next_byte());
				}
				buff.into_iter().flatten().collect::<Vec<u8>>()
			};
			if &maybe_endif[..] == b"endif" {
				self.emit();
				self.step_condition_end();
			} else {
				self.push_buffer(&maybe_endif);
				self.drain();
			}
		}
	}

	fn step_condition(&mut self) {
		while let Some(b) = self.next_byte() {
			match b {
				b'>' => break,
				b'-' => {
					if self.predict(b"->") {
						// just a comment. 
						self.shift(2);
						self.drain_expr();
						self.push_buffer(b"]-->");
						return;
					} else {
						self.push_buffer(b"-");
					}
				}
				b']' => continue,
				t => self.push_expr(&[t]),
			}
		}
		
		if self.current_expr.is_empty() {
			self.drain();
		}

		self.clear_buffer();
	}

	fn step_condition_end(&mut self) {
		while let Some(t) = self.next_byte() {
			match t {
				b'>' => break,
				b'-' => {
					if self.predict(b"->") {
						// just a comment. 
						self.shift(2);
						self.drain_expr();
						self.push_buffer(b"endif]-->");
						return;
					} else {
						self.push_text(b"-");
					}
				}
				_ => continue,
			}
		}

		self.clear_buffer();
	}
}

