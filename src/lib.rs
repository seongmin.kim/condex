#[macro_use]
extern crate lalrpop_util;
lalrpop_mod!(pub condex);
pub(crate) use crate::condex::ExprParser;

mod ast;
mod parser;
mod interface;
pub use interface::Condex;

#[cfg(test)]
mod tests;
